# Sliter.io Bots

## Screenshot

<p align="center">
    <img src="screenshot.jpg">
</p>

## [Demo Gif](SlitherBotsDemo.gif)

## Usage

Open Firefox (or Chrome for you heathens).

Go to http://slither.io

View -> Developer -> Javascript Console

Open a bot, copy the entire file, paste it into the JS console, hit enter, watch and be amazed.

Copy the bot to a new file, make modifications, ????, profit!

# Potential Modifications

* Prediction of where other bots are going to move
* Actively try and kill other snakes
* Use Machine Learning?

# Snippets

Get distance from snake to click point:
```
Math.sqrt(Math.pow(xm,2)+Math.pow(ym,2))
```
